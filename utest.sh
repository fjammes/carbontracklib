#!/bin/bash

set -euxo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)

echo "Run unit tests"
cd $DIR
go test ./travel
