package travel

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// Testfiletype check return values for metadata.filetype()
func TestGeodesicDist(t *testing.T) {

	lat1 := 0.
	lon1 := 0.
	lat2 := 0.
	lon2 := 0.
	expectedDistance := 0.
	distance := GeodesicDist(lat1, lon1, lat2, lon2)

	assert.Equal(t, distance, expectedDistance, "The two distances should be the same.")

	latParis := 48.856614
	lonParis := 2.3522219

	latMarseille := 43.296482
	lonMarseille := 5.36978

	expectedDistance = 660.4805769123861
	distance = GeodesicDist(latParis, lonParis, latMarseille, lonMarseille)
	assert.Equal(t, distance, expectedDistance, "The two distances should be the same.")

	latMoscow := 55.755826
	lonMoscow := 37.6173

	expectedDistance = 2486.2462608784795
	distance = GeodesicDist(latParis, lonParis, latMoscow, lonMoscow)
	assert.Equal(t, distance, expectedDistance, "The two distances should be the same.")

}
