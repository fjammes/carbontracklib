# carbontracklib 

Tools to compute carbon emissions

## WASM

Build WASM binary

```shell
cd wasm
build.wasm.sh
# This will produce a carbontrack.wasm file
```
