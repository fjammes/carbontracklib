package main

import (
	"fmt"

	"framagit.org/fjammes/carbontracklib/v2/travel"
)

func main() {
	lat1 := 0.
	lon1 := 0.
	lat2 := 0.
	lon2 := 0.
	expectedDistance := 0
	distance := travel.DistanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2)

	fmt.Printf("Distance %s %s", distance, expectedDistance)
}
