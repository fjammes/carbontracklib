#!/bin/bash

set -euxo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)

WASM_BIN="$DIR/carbontrack.wasm"
DEST_DIR="$DIR/../../l1p5-vuejs/public/static/wasm"

GOOS=js GOARCH=wasm go build -o "$WASM_BIN" $DIR/main.go

echo "TODO add parameter for deployment destination"
echo "Deploy WASM binary to $DEST_DIR"
sudo cp "$WASM_BIN" "$DEST_DIR"
