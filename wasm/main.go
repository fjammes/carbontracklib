package main

import (
	"log"
	"syscall/js"

	"framagit.org/fjammes/carbontracklib/v2/travel"
)

// func sub(a, b float64) float64

func geodesicDist(this js.Value, inputs []js.Value) interface{} {

	lat1 := inputs[0].Float()
	lon1 := inputs[1].Float()
	lat2 := inputs[2].Float()
	lon2 := inputs[3].Float()
	log.Printf("Coordinates: %f, %f, %f, %f", lat1, lon1, lat2, lon2)
	return travel.GeodesicDist(lat1, lon1, lat2, lon2)

}

func main() {
	c := make(chan int) // channel to keep the wasm running, it is not a library as in rust/c/c++, so we need to keep the binary running
	js.Global().Set("geodesicDist", js.FuncOf(geodesicDist))

	<-c // pause the execution so that the resources we create for JS keep available
}
